class Globel{
  static List<dynamic> horizonLayout =  [{
    "layout": "logo",
    "showLogo": true,
    "showSearch": false,
    "showWishlist": true,
    "showMenu": true,
    "showCart": false,
    "iconColor": "#000000",
    "showNotification": true,
    "menuIcon": {
      "name": "line_horizontal_3",
      "fontFamily": "CupertinoIcons"
    },
    "pos": 100
  }];
}