// ignore_for_file: prefer_single_quotes, lines_longer_than_80_chars final
Map<String, dynamic> environment = {
  "version": "2.2.0",
  "appConfig": "lib/config/config_en.json",
  "serverConfig": {
  /*"url": "https://thehomeentrepreneurs.com",*/
    "url": "https://stage.thehomeentrepreneurs.com",
  "type": "magento",
  "accessToken": "agh98rzon2i8ac54hnpnerdcb2u26g0m"
  },
  "defaultDarkTheme": false,
  "enableRemoteConfigFirebase": false,
  "loginSMSConstants": {
    "countryCodeDefault": "VN",
    "dialCodeDefault": "+84",
    "nameDefault": "UAE"
  },
  "storeIdentifier": {
    "disable": true,
    "android": "io.thehome.starter",
    "ios": "1469772800"
  },
  "advanceConfig": {
    "DefaultLanguage": "en",
    "DetailedBlogLayout": "halfSizeImageType",
    "EnablePointReward": true,
    "hideOutOfStock": false,
    "HideEmptyTags": true,
    "HideEmptyCategories": true,
    "EnableRating": true,
    "hideEmptyProductListRating": true,
    "EnableCart": true,
    "EnableSkuSearch": true,
    "showStockStatus": true,
    "GridCount": 3,
    "isCaching": false,
    "kIsResizeImage": false,
    "httpCache": true,
    "DefaultCurrency": {
      "symbol": "AED",
      "decimalDigits": 2,
      "symbolBeforeTheNumber": true,
      "currency": "AED",
      "currencyCode": "aed",
      "smallestUnitRate": 100
    },
    "Currencies": [
      {
        "symbol": "AED",
        "decimalDigits": 0,
        "symbolBeforeTheNumber": true,
        "currency": "AED",
        "currencyCode": "AED"
      },
      {
        "symbol": "\$",
        "decimalDigits": 2,
        "symbolBeforeTheNumber": true,
        "currency": "USD",
        "currencyCode": "USD",
        "smallestUnitRate": 100
      }
    ],
    "DefaultStoreViewCode": "",
    "EnableAttributesConfigurableProduct": ["color", "size", "power", "multi_color"],
    "EnableAttributesLabelConfigurableProduct": ["color", "size", "power", "variations"],
    "isMultiLanguages": false,
    "EnableApprovedReview": false,
    "EnableSyncCartFromWebsite": true,
    "EnableSyncCartToWebsite": true,
    "EnableFirebase": true,
    "RatioProductImage": 1.2,
    "EnableCouponCode": true,
    "ShowCouponList": true,
    "ShowAllCoupons": true,
    "ShowExpiredCoupons": true,
    "AlwaysShowTabBar": true,
    "PrivacyPoliciesPageId": 25569,
    "PrivacyPoliciesPageUrl": "https://thehomeentrepreneurs.com/privacy-policy",
    "AutoDetectLanguage": false,
    "QueryRadiusDistance": 10,
    "MinQueryRadiusDistance": 1,
    "MaxQueryRadiusDistance": 10,
    "EnableMembershipUltimate": false,
    "EnableDeliveryDateOnCheckout": false,
    "EnableNewSMSLogin": false,
    "EnableBottomAddToCart": false
  },
  "defaultDrawer": {
    "logo": "assets/images/logo.png",
    "background": null,
    "items": [
      {"type": "home", "show": true},
      {"type": "blog", "show": true},
      {"type": "categories", "show": true},
      {"type": "cart", "show": true},
      {"type": "profile", "show": true},
      {"type": "login", "show": true},
      {"type": "category", "show": true}
    ]
  },
  "defaultSettings": [
    "products",
    "chat",
    "wishlist",
    "notifications",
    "darkTheme",
    "order",
    "point",
    "rating",
    "privacy",
    "about",
  ],
  "loginSetting": {
    "IsRequiredLogin": false,
    "showAppleLogin": true,
    "showFacebook": true,
    "showSMSLogin": false,
    "showGoogleLogin": true,
    "showPhoneNumberWhenRegister": false,
    "requirePhoneNumberWhenRegister": false,
    "isResetPasswordSupported": true,
    "facebookAppId": "309472237658772",
    "facebookLoginProtocolScheme": "fb309472237658772"
  },
  "oneSignalKey": {
    "enable": false,
    "appID": "8b45b6db-7421-45e1-85aa-75e597f62714"
  },
  "onBoardingData": [
    {
      "title": "Easy Shopping",
      "desc": "1-2-& Order Done, It is as simple as that!",
      "image": "assets/images/home/onb1.png"
    },
    {
      "title": "Secure Payment",
      "desc": "Highly Secure Platform for your shopping because we care!",
      "image": "assets/images/home/onb2.png"
    },
    {
      "title": "Quick Delivery",
      "desc":
      "Easy Shopping & Quick delivery is what makes THE your perfect choice",
      "image": "assets/images/home/onb3.png"
    }
  ],
  "vendorOnBoardingData": [
    {
      "title": "Welcome aboard",
      "image": "assets/images/searching.png",
      "desc": "Just a few more steps to become our vendor"
    },
    {
      "title": "Let's Get Started",
      "image": "assets/images/manage.png",
      "desc": "Good Luck for great beginnings."
    }
  ],
  "adConfig": {
    "enable": false,
    "facebookTestingId": "",
    "googleTestingId": [],
    "ads": [
      {
        "type": "banner",
        "provider": "google",
        "iosId": "ca-app-pub-3940256099942544/2934735716",
        "androidId": "ca-app-pub-3940256099942544/6300978111",
        "showOnScreens": ["home", "search"],
        "waitingTimeToDisplay": 2
      },
      {
        "type": "banner",
        "provider": "google",
        "iosId": "ca-app-pub-2101182411274198/5418791562",
        "androidId": "ca-app-pub-2101182411274198/4052745095"
      },
      {
        "type": "interstitial",
        "provider": "google",
        "iosId": "ca-app-pub-3940256099942544/4411468910",
        "androidId": "ca-app-pub-3940256099942544/4411468910",
        "showOnScreens": ["profile"],
        "waitingTimeToDisplay": 5
      },
      {
        "type": "reward",
        "provider": "google",
        "iosId": "ca-app-pub-3940256099942544/1712485313",
        "androidId": "ca-app-pub-3940256099942544/4411468910",
        "showOnScreens": ["cart"]
      },
      {
        "type": "banner",
        "provider": "facebook",
        "iosId": "IMG_16_9_APP_INSTALL#430258564493822_876131259906548",
        "androidId": "IMG_16_9_APP_INSTALL#430258564493822_489007588618919",
        "showOnScreens": ["home"]
      },
      {
        "type": "interstitial",
        "provider": "facebook",
        "iosId": "430258564493822_489092398610438",
        "androidId": "IMG_16_9_APP_INSTALL#430258564493822_489092398610438"
      }
    ],
    "adMobAppId": "ca-app-pub-7432665165146018~2664444130"
  },
  "firebaseDynamicLinkConfig": {
    "isEnabled": true,
    "shortDynamicLinkEnable": false,
    "uriPrefix": "https://thehomeentrepreneurs.page.link/products",
    "link": "https://thehomeentrepreneurs.com/",
    "androidPackageName": "io.thehome.starter",
    "androidAppMinimumVersion": 1,
    "iOSBundleId": "io.thehomeentrepreneurs.starter",
    "iOSAppMinimumVersion": "1.0.1",
    "iOSAppStoreId": "1599210909"
  },
  "languagesInfo": [
    {
      "name": "English",
      "icon": "assets/images/country/gb.png",
      "code": "en",
      "text": "English",
      "storeViewCode": ""
    }
  ],
  "unsupportedLanguages": ["ku"],
  "paymentConfig": {
    "DefaultCountryISOCode": "AE",
    "DefaultStateISOCode": "",
    "EnableShipping": true,
    "EnableAddress": true,
    "EnableCustomerNote": true,
    "EnableAddressLocationNote": false,
    "EnableAlphanumericZipCode": false,
    "EnableReview": true,
    "allowSearchingAddress": true,
    "GoogleApiKey": "AIzaSyBmrCdHJ7X-H2R_FIfjq3jh4Ev6Eub5V18",
    "GuestCheckout": true,
    "EnableOnePageCheckout": false,
    "NativeOnePageCheckout": false,
    "CheckoutPageSlug": {"en": "checkout"},
    "EnableCreditCard": false,
    "UpdateOrderStatus": false,
    "ShowOrderNotes": true,
    "EnableRefundCancel": true,
    "RefundPeriod": 7
  },
  "payments": {
    "paypal": "assets/icons/payment/paypal.png",
    "stripe": "assets/icons/payment/stripe.png",
    "razorpay": "assets/icons/payment/razorpay.png",
    "tap": "assets/icons/payment/tap.png"
  },
  "stripeConfig": {
    "serverEndpoint": "https://stripe-server.vercel.app",
    "publishableKey": "pk_test_MOl5vYzj1GiFnRsqpAIHxZJl",
    "enabled": true,
    "paymentMethodId": "stripe",
    "returnUrl": "starter://thehome.com",
    "enableManualCapture": false
  },
  "paypalConfig": {
    "clientId":
    "ASlpjFreiGp3gggRKo6YzXMyGM6-NwndBAQ707k6z3-WkSSMTPDfEFmNmky6dBX00lik8wKdToWiJj5w",
    "secret":
    "ECbFREri7NFj64FI_9WzS6A0Az2DqNLrVokBo0ZBu4enHZKMKOvX45v9Y1NBPKFr6QJv2KaSp5vk5A1G",
    "production": false,
    "paymentMethodId": "paypal",
    "enabled": true
  },
  "razorpayConfig": {
    "keyId": "rzp_test_SDo2WKBNQXDk5Y",
    "keySecret": "RrgfT3oxbJdaeHSzvuzaJRZf",
    "paymentMethodId": "razorpay",
    "enabled": true
  },
  "tapConfig": {
    "SecretKey": "sk_test_XKokBfNWv6FIYuTMg5sLPjhJ",
    "paymentMethodId": "tap",
    "enabled": true
  },
  "mercadoPagoConfig": {
    "accessToken":
    "TEST-5726912977510261-102413-65873095dc5b0a877969b7f6ffcceee4-613803978",
    "production": false,
    "paymentMethodId": "woo-mercado-pago-basic",
    "enabled": true
  },
  "payTmConfig": {
    "paymentMethodId": "paytm",
    "merchantId": "your-merchant-id",
    "production": false,
    "enabled": true
  },
  "defaultCountryShipping": [],
  "afterShip": {
    "api": "e2e9bae8-ee39-46a9-a084-781d0139274f",
    "tracking_url": "https://starter.aftership.com"
  },
  "productDetail": {
    "height": 0.4,
    "marginTop": 0,
    "safeArea": true,
    "showVideo": true,
    "showBrand": true,
    "showThumbnailAtLeast": 1,
    "layout": "simpleType",
    "borderRadius": 3.0,
    "ShowSelectedImageVariant": true,
    "ForceWhiteBackground": false,
    "AutoSelectFirstAttribute": true,
    "enableReview": true,
    "attributeImagesSize": 50.0,
    "showSku": true,
    "showStockQuantity": true,
    "showProductCategories": true,
    "showProductTags": true,
    "hideInvalidAttributes": true,
    "showQuantityInList": false,
    "showAddToCartInSearchResult": false,
    "productListItemHeight": 125
  },
  "blogDetail": {
    "showComment": true,
    "showHeart": true,
    "showSharing": true,
    "showTextAdjustment": true,
    "enableAudioSupport": false
  },
  "productVariantLayout": {
    "color": "color",
    "size": "box",
    "height": "option",
    "color-image": "image",
    "multi_color" : "multi_color"
  },
  "productAddons": {
    "allowImageType": true,
    "allowVideoType": true,
    "allowCustomType": true,
    "allowedCustomType": ["png", "pdf", "docx"],
    "allowMultiple": false,
    "fileUploadSizeLimit": 5.0
  },
  "cartDetail": {"minAllowTotalCartValue": 0, "maxAllowQuantity": 10},
  "productVariantLanguage": {
    "en": {
      "color": "Color",
      "size": "Size",
      "height": "Height",
      "color-image": "Color"
    },
    "ar": {
      "color": "اللون",
      "size": "بحجم",
      "height": "ارتفاع",
      "color-image": "اللون"
    },
    "vi": {
      "color": "Màu",
      "size": "Kích thước",
      "height": "Chiều Cao",
      "color-image": "Màu"
    }
  },
  "excludedCategory": 311,
  "saleOffProduct": {
    "ShowCountDown": true,
    "HideEmptySaleOffLayout": false,
    "Color": "#C7222B"
  },
  "notStrictVisibleVariant": true,
  "configChat": {
    "EnableSmartChat": true,
    "showOnScreens": ["profile"],
    "hideOnScreens": [],
    "version": "2"
  },
  "smartChat": [
    {
      "app": "https://wa.me/+9710543282651",
      "iconData": "whatsapp",
      "description": "WhatsApp"
    },
    {"app": "tel:+97143240667", "iconData": "phone", "description": "Call Us"},
  ],
  "adminEmail": "info@thehomeentrepreneurs.com",
  "adminName": "THE",
  "vendorConfig": {
    "VendorRegister": true,
    "DisableVendorShipping": false,
    "ShowAllVendorMarkers": true,
    "DisableNativeStoreManagement": false,
    "dokan": "my-account?vendor_admin=true",
    "wcfm": "store-manager?vendor_admin=true",
    "DisableMultiVendorCheckout": false,
    "DisablePendingProduct": false
  },
  "deliveryConfig": {"DisableDeliveryManagement": false},
  "loadingIcon": {"size": 30.0, "type": "theCustom"},
  "splashScreen": {
    "enable": true,
    "duration": 4400,
    "type": "static",
    "image": "assets/images/the-logo.png",
    "animationName": "fluxstore",
 },
  "darkConfig": {"MainColor": "#0261BD", "logo": "assets/images/the-logo.png"},
  "lightConfig": {"MainColor": "#0261BD", "logo": "assets/images/the-logo.png"}
};
