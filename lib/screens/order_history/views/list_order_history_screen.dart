import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../../../common/constants.dart';
import '../../../common/constants/globel.dart';
import '../../../generated/l10n.dart';
import '../../../models/index.dart';
import '../../../modules/dynamic_layout/config/logo_config.dart';
import '../../../modules/dynamic_layout/logo/logo.dart';
import '../../../widgets/common/paging_list.dart';
import '../models/list_order_history_model.dart';
import '../models/order_history_detail_model.dart';
import 'widgets/order_list_item.dart';
import 'widgets/order_list_loading_item.dart';

class ListOrderHistoryScreen extends StatefulWidget {
  @override
  _ListOrderHistoryScreenState createState() => _ListOrderHistoryScreenState();
}

class _ListOrderHistoryScreenState extends State<ListOrderHistoryScreen> {
  ListOrderHistoryModel get listOrderViewModel =>
      Provider.of<ListOrderHistoryModel>(context, listen: false);

  var mapOrderHistoryDetailModel = <int, OrderHistoryDetailModel>{};

  @override
  Widget build(BuildContext context) {
    final user = Provider.of<UserModel>(context, listen: false).user ?? User();

    Map logoConfig = Globel.horizonLayout.firstWhere(
        (element) => element['layout'] == 'logo',
        orElse: () => Map<String, dynamic>.from({}));
    var config = LogoConfig.fromJson(logoConfig);

    return Scaffold(
        backgroundColor: Theme.of(context).backgroundColor,
        appBar: AppBar(
            centerTitle: false,
            automaticallyImplyLeading: false,
            titleSpacing: 0,
            title: Text(S.of(context).orderHistory,
                style: Theme.of(context).textTheme.headline2!.copyWith(
                      color: Colors.black,
                      fontSize: 21,
                    )),
            backgroundColor: Theme.of(context).backgroundColor,
            actions: [
              SizedBox(
                height: 30,
                width: 30,
                child: LogoIcon(
                    menuIcon: config.wishlistIcon ?? MenuIcon(name: 'heart'),
                    onTap: () {
                      Navigator.of(context).pushNamed(RouteList.wishlist);
                    },
                    config: config),
              ),
              SizedBox(
                  height: 30,
                  width: 30,
                  child: LogoIcon(
                      menuIcon:
                          config.notificationIcon ?? MenuIcon(name: 'bell'),
                      onTap: () {
                        Navigator.of(context).pushNamed(RouteList.notify);
                      },
                      config: config)),
              SizedBox(width: 10, height: 10, child: Container())
            ],
            leading: IconButton(
                icon: const Icon(Icons.arrow_back, color: Colors.black),
                onPressed: () => Navigator.of(context).pop())),
        body: PagingList<ListOrderHistoryModel, Order>(
            onRefresh: mapOrderHistoryDetailModel.clear,
            itemBuilder: (_, order, index) {
              if (mapOrderHistoryDetailModel[index] == null) {
                final orderHistoryDetailModel =
                    OrderHistoryDetailModel(order: order, user: user);
                mapOrderHistoryDetailModel[index] = orderHistoryDetailModel;
              }
              return ChangeNotifierProvider<OrderHistoryDetailModel>.value(
                  value: mapOrderHistoryDetailModel[index]!,
                  child: OrderListItem());
            },
            lengthLoadingWidget: 3,
            loadingWidget: const OrderListLoadingItem()));
  }
}
