import 'package:flutter/material.dart';
import 'package:fstore/screens/common/app_bar_mixin.dart';
import 'package:provider/provider.dart';

import '../../common/config.dart';
import '../../common/constants.dart';
import '../../common/constants/globel.dart';
import '../../generated/l10n.dart';
import '../../menu/appbar.dart';
import '../../menu/sidebar.dart';
import '../../models/index.dart' show AppModel, Category, CategoryModel;
import '../../modules/dynamic_layout/config/app_config.dart';
import '../../modules/dynamic_layout/config/logo_config.dart';
import '../../modules/dynamic_layout/logo/logo.dart';
import '../../services/index.dart';
import '../../widgets/common/flux_image.dart';
import 'layouts/column.dart';
import 'layouts/grid.dart';
import 'layouts/side_menu.dart';
import 'layouts/side_menu_with_sub.dart';
import 'layouts/sub.dart';

class CategoriesScreen extends StatefulWidget {
  final bool showSearch;

  const CategoriesScreen({
    Key? key,
    this.showSearch = true,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() {
    return CategoriesScreenState();
  }
}

class CategoriesScreenState extends State<CategoriesScreen>
    with AutomaticKeepAliveClientMixin, SingleTickerProviderStateMixin {
  @override
  bool get wantKeepAlive => true;

  late FocusNode _focus;
  bool isVisibleSearch = false;
  String? searchText;
  var textController = TextEditingController();

  late Animation<double> animation;
  late AnimationController controller;

  AppBarConfig? get appBar =>
      context.select((AppModel model) => model.appConfig?.appBar);

  @override
  void initState() {
    super.initState();
    controller = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this);
    animation = Tween<double>(begin: 0, end: 60).animate(controller);
    animation.addListener(() {
      setState(() {});
    });

    _focus = FocusNode();
    _focus.addListener(_onFocusChange);
  }

  void _onFocusChange() {
    if (_focus.hasFocus && animation.value == 0) {
      controller.forward();
      setState(() {
        isVisibleSearch = true;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    super.build(context);

    final category = Provider.of<CategoryModel>(context);
    final appModel = Provider.of<AppModel>(context);

    Map logoConfig = Globel.horizonLayout
        .firstWhere((element) => element['layout'] == 'logo', orElse: () => Map<String, dynamic>.from({}));
    var config = LogoConfig.fromJson(logoConfig);



    return Scaffold(
     /* appBar: (appBar?.shouldShowOn(RouteList.category) ?? true)
          ? AppBar(
              titleSpacing: 0,
              elevation: 0,
              automaticallyImplyLeading: false,
              backgroundColor: Theme.of(context).backgroundColor,
              title: FluxAppBar(),
            )
          : null,*/
      appBar: AppBar(
          backgroundColor: Theme.of(context).backgroundColor,
        iconTheme: const IconThemeData(color: Colors.black),
        title: Builder(builder: (context) => FluxImage(imageUrl: appModel.themeConfig.logo, height: 50)),
        titleSpacing: 0,
        actions: [
          SizedBox(
            height: 30,
            width: 30,
            child: LogoIcon(
                menuIcon: config.wishlistIcon ?? MenuIcon(name: 'heart'),
                onTap: () {
                  Navigator.of(context).pushNamed(RouteList.wishlist);
                },
                config: config),
          ),
          SizedBox(
              height: 30,
              width: 30,
              child: LogoIcon(
                  menuIcon: config.notificationIcon ?? MenuIcon(name: 'bell'),
                  onTap: () {
                    Navigator.of(context).pushNamed(RouteList.notify);
                  },
                  config: config)),
          SizedBox(width: 10, height: 10, child: Container())
        ],
         /* title: Text(
            S.of(context).search,
            style: Theme.of(context)
                .textTheme
                .headline5
                ?.copyWith(fontWeight: FontWeight.w700),
          )*/
      ),
      drawer: Theme(
          data: Theme.of(context).copyWith(canvasColor: Colors.white),
          child: const Drawer(
            child: SideBarMenu(),
          )),
      //drawer: const SideBarMenu(),
      backgroundColor: Theme.of(context).backgroundColor,
      body: ListenableProvider.value(
        value: category,
        child: Consumer<CategoryModel>(
          builder: (context, value, child) {
            if (value.isLoading) {
              return kLoadingWidget(context);
            }

            if (value.categories == null) {
              return Container(
                width: double.infinity,
                height: double.infinity,
                alignment: Alignment.center,
                child: Text(S.of(context).dataEmpty),
              );
            }

            var categories = value.categories;

            return SafeArea(
              bottom: false,
              child: [
                GridCategory.type,
                ColumnCategories.type,
                SideMenuCategories.type,
                SubCategories.type,
                SideMenuSubCategories.type
              ].contains(appModel.categoryLayout)
                  ? Column(
                      children: <Widget>[
                        renderHeader(),
                        Expanded(
                          child: renderCategories(
                              categories, appModel.categoryLayout),
                        )
                      ],
                    )
                  : ListView(
                      children: <Widget>[
                        renderHeader(),
                        renderCategories(categories, appModel.categoryLayout)
                      ],
                    ),
            );
          },
        ),
      ),
    );
  }

  Widget renderHeader() {
    final screenSize = MediaQuery.of(context).size;
    return SizedBox(
      width: screenSize.width,
      child: FittedBox(
        fit: BoxFit.cover,
        child: SizedBox(
          width:
              screenSize.width / (2 / (screenSize.height / screenSize.width)),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              if (Navigator.canPop(context))
                GestureDetector(
                  onTap: () => Navigator.pop(context),
                  child: const Padding(
                    padding: EdgeInsets.symmetric(horizontal: 10),
                    child: Icon(
                      Icons.arrow_back_ios,
                    ),
                  ),
                ),
              Padding(
                padding: const EdgeInsets.only(
                    top: 10, left: 10, bottom: 10, right: 10),
                child: Text(
                  S.of(context).category,
                  style: Theme.of(context)
                      .textTheme
                      .headline5!
                      .copyWith(fontWeight: FontWeight.w700),
                ),
              ),
              if (widget.showSearch)
                IconButton(
                  icon: Icon(
                    Icons.search,
                    color: Theme.of(context)
                        .colorScheme
                        .secondary
                        .withOpacity(0.6),
                  ),
                  onPressed: () {
                    Navigator.of(context).pushNamed(RouteList.homeSearch);
                  },
                ),
            ],
          ),
        ),
      ),
    );
  }

  Widget renderCategories(List<Category>? categories, String layout) {
    return Services().widget.renderCategoryLayout(categories, layout);
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }
}
