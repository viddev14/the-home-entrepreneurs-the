import 'dart:collection';

import 'package:fstore/common/constants/globel.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:inspireui/widgets/deferred_widget.dart';
import 'package:provider/provider.dart';
import 'package:inspireui/icons/icon_picker.dart' deferred as defer_icon;
import '../../../common/config.dart';
import '../../../common/constants.dart';
import '../../../models/app_model.dart';
import '../../../models/index.dart' show Product, ProductModel, UserModel;
import '../../../modules/dynamic_layout/config/app_config.dart';
import '../../../modules/dynamic_layout/config/logo_config.dart';
import '../../../modules/dynamic_layout/logo/logo.dart';
import '../../../services/index.dart';
import '../../../widgets/product/product_bottom_sheet.dart';
import '../../../widgets/product/widgets/heart_button.dart';
import '../../chat/vendor_chat.dart';
import '../product_detail_screen.dart';
import '../widgets/index.dart';
import '../widgets/product_image_slider.dart';


class SimpleLayout extends StatefulWidget {
  final Product product;
  final bool isLoading;

  const SimpleLayout({required this.product, this.isLoading = false});

  @override
  // ignore: no_logic_in_create_state
  _SimpleLayoutState createState() => _SimpleLayoutState(product: product);
}

class _SimpleLayoutState extends State<SimpleLayout> with SingleTickerProviderStateMixin {
  final _scrollController = ScrollController();
  late Product product;

  _SimpleLayoutState({required this.product});

  Map<String, String> mapAttribute = HashMap();
  var _hideController;
  var top = 0.0;

  @override
  void initState() {
    super.initState();
    _hideController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 450),
      value: 1.0,
    );
  }

  @override
  void didUpdateWidget(SimpleLayout oldWidget) {
    if (oldWidget.product.type != widget.product.type) {
      setState(() {
        product = widget.product;
      });
    }
    super.didUpdateWidget(oldWidget);
  }

  /// Render product default: booking, group, variant, simple, booking
  Widget renderProductInfo() {
    var body;

    if (widget.isLoading == true) {
      body = kLoadingWidget(context);
    } else {
      switch (product.type) {
        case 'appointment':
          return Services().getBookingLayout(product: product);
        case 'booking':
          body = ListingBooking(product);
          break;
        case 'grouped':
          body = GroupedProduct(product);
          break;
        default:
          body = ProductVariant(product);
      }
    }

    return SliverToBoxAdapter(
      child: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15.0),
        child: body,
      ),
    );
  }

   @override
  Widget build(BuildContext context) {
    final size = MediaQuery.of(context).size;
    final widthHeight = size.height;

    Map logoConfig = Globel.horizonLayout
        .firstWhere((element) => element['layout'] == 'logo', orElse: () => Map<String, dynamic>.from({}));
    var config = LogoConfig.fromJson(logoConfig);

    final userModel = Provider.of<UserModel>(context, listen: false);
    return Container(
      color: Theme.of(context).backgroundColor,
      child: SafeArea(
        bottom: false,
        top: kProductDetail.safeArea,
        child: ChangeNotifierProvider(
          create: (_) => ProductModel(),
          child: Stack(
            children: <Widget>[
              Scaffold(
                floatingActionButton: (!Config().isVendorType() || !kConfigChat['EnableSmartChat'])
                    ? null
                    : Padding(
                        padding: const EdgeInsets.only(bottom: 30),
                        child: VendorChat(
                          user: userModel.user,
                          store: product.store,
                        ),
                      ),
                backgroundColor: Theme.of(context).backgroundColor,
                body: CustomScrollView(
                  controller: _scrollController,
                  slivers: <Widget>[
                    SliverAppBar(
                      leading: IconButton(
                        icon: Icon(Icons.arrow_back, size: 20, color: Colors.black),
                        onPressed: () {
                          if (kIsWeb) {
                            eventBus.fire(const EventOpenCustomDrawer());
                            // LayoutWebCustom.changeStateMenu(true);
                          }
                          Navigator.pop(context);
                        },
                      ),
                    systemOverlayStyle: SystemUiOverlayStyle.light,
                    backgroundColor: Theme.of(context).backgroundColor,
                    elevation: 1.0,
                    //expandedHeight: kIsWeb ? 0 : widthHeight * kProductDetail.height,
                    pinned: true,
                    floating: false,
                      title: Text(
                        "Product Detail",
                        style: Theme.of(context).textTheme.headline2!.copyWith(
                          color: Colors.black,
                          fontSize: 21,
                        ),
                      ),
                        titleSpacing: 0,
                        actions: [
                          SizedBox(
                            height: 30,
                            width: 30,
                            child: LogoIcon(
                                menuIcon: config.wishlistIcon ?? MenuIcon(name: 'heart'),
                                onTap: () {
                                  Navigator.of(context).pushNamed(RouteList.wishlist);
                                },
                                config: config),
                          ),
                          SizedBox(
                              height: 30,
                              width: 30,
                              child: LogoIcon(
                                  menuIcon: config.notificationIcon ?? MenuIcon(name: 'bell'),
                                  onTap: () {
                                    Navigator.of(context).pushNamed(RouteList.notify);
                                  },
                                  config: config)),
                          SizedBox(width: 10, height: 10, child: Container())
                        ],
                      //flexibleSpace:kIsWeb ? const SizedBox() : ProductImageSlider(product: product),
                    ),
                    SliverAppBar(
                      automaticallyImplyLeading: false,
                      expandedHeight: kIsWeb ? 0 : widthHeight * kProductDetail.height,
                      flexibleSpace:kIsWeb ? const SizedBox() : ProductImageSlider(product: product)
                    ),
                    SliverList(
                      delegate: SliverChildListDelegate(
                        <Widget>[
                          const SizedBox(
                            height: 2,
                          ),
                          if (kIsWeb)
                            ProductGallery(
                              product: widget.product,
                            ),
                          Padding(
                            padding: const EdgeInsets.only(
                              top: 8.0,
                              bottom: 4.0,
                              left: 15,
                              right: 15,
                            ),
                            child: product.type == 'grouped' ? const SizedBox() : ProductTitle(product),
                          ),
                        ],
                      ),
                    ),
                    if (kEnableShoppingCart) renderProductInfo(),
                    if (!kEnableShoppingCart &&
                        product.shortDescription != null &&
                        product.shortDescription!.isNotEmpty)
                      SliverToBoxAdapter(
                        child: Padding(
                          padding: const EdgeInsets.symmetric(horizontal: 15.0),
                          child: ProductShortDescription(product),
                        ),
                      ),
                    SliverToBoxAdapter(
                      child: Padding(
                        padding: const EdgeInsets.symmetric(
                          // horizontal: 15.0,
                          vertical: 8.0,
                        ),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: [
                            Padding(
                              padding: const EdgeInsets.symmetric(
                                horizontal: 15.0,
                              ),
                              child: Column(
                                children: [
                                  Services().widget.renderVendorInfo(product),
                                  ProductDescription(product),
                                  if (kProductDetail.showProductCategories) ProductDetailCategories(product),
                                  if (kProductDetail.showProductTags) ProductTag(product),
                                  Services().widget.productReviewWidget(product.id!),
                                ],
                              ),
                            ),
                            RelatedProduct(product),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              if (kEnableShoppingCart)
                Align(
                  alignment: Alignment.bottomRight,
                  child: ExpandingBottomSheet(
                    hideController: _hideController,
                  ),
                ),
            ],
          ),
        ),
      ),
    );
  }
}
